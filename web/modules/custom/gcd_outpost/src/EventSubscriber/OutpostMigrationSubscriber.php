<?php declare(strict_types = 1);

namespace Drupal\gcd_outpost\EventSubscriber;

use Drupal\Core\Serialization\Yaml;
use Drupal\localgov_outpost_connector\Event\MigrationDeriverEvent;
use Drupal\localgov_outpost_connector\Event\OutpostConnectorEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Make Greenwich Outpost specific changes to the import migrations.
 */
final class OutpostMigrationSubscriber implements EventSubscriberInterface {

  /**
   * Kernel response event handler.
   */
  public function migrationDeriver(MigrationDeriverEvent $event): void {
    // @todo this could be generalised. Even something like any yml in a
    // directory with the correct name to override that migration.
    if ($event->getMigrationId() == 'localgov_outpost_services') {
      $yaml = (<<<EOL
process:
  gcd_cqc_id/id: meta__cqc_location_id
  gcd_cqc_id/type:
    plugin: default_value
    default_value: 'location'
  gcd_referral_required:
    plugin: static_map
    source: meta__referral_needed
    default_value: null
    map:
      'Yes': true
      'No': false
  gcd_referral_details: meta__if_a_referral_is_needed_please_tell_us_who_needs_to_refer_them_or_if_they_need_an_assessment
  gcd_book_online:
    plugin: static_map
    source: meta__book_online
    default_value: null
    map:
      'Yes': true
      'No': false
  gcd_book_other:
    plugin: static_map
    source: meta__call_or_email_to_arrange
    default_value: null
    map:
      'Yes': true
      'No': false
    default_value: false
  gcd_booking_required:
    plugin: static_map
    source: meta__no_booking_required
    default_value: null
    map:
      'Yes': true
      'No': false
EOL);
      $overrides = Yaml::decode($yaml);
      $migration = $event->getMigration();
      // Rather than merging recursively, merge each section and allow
      // overriding keys.
      foreach ($migration as $section => $values) {
        if (isset($overrides[$section])) {
          $values = array_merge($values, $overrides[$section]);
          $migration[$section] = $values;
        }
      }
      $event->setMigration($migration);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      OutpostConnectorEvents::MIGRATION_DERIVER => ['migrationDeriver'],
    ];
  }

}
