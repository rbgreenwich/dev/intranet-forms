<?php declare(strict_types = 1);

namespace Drupal\office_hours_search_api\EventSubscriber;

use Drupal\search_api\Event\MappingFieldTypesEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Add opening hours to recognised fields.
 */
final class SearchApiFieldsSubscriber implements EventSubscriberInterface {

  /**
   * Reacts to the field type mapping alter event.
   *
   * @param \Drupal\search_api\Event\MappingFieldTypesEvent $event
   *   The field type mapping alter event.
   */
  public function fieldTypeMappingAlter(MappingFieldTypesEvent $event) {
    $mapping = &$event->getFieldTypeMapping();
    $mapping['field_item:office_hours'] = 'string';
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SearchApiEvents::MAPPING_FIELD_TYPES => 'fieldTypeMappingAlter',
    ];
  }

}
