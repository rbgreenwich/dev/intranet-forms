<?php

// greenwich_blocks/src/Plugin/Block/GreenwichHeaderBlock.php
namespace Drupal\greenwich_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Menu\MenuTreeParameters;


/**
 * Provides a global site header block.
 *
 * @Block(
 *   id = "greenwich_site_header",
 *   admin_label = @Translation("Greenwich: Site Header"),
 *   category = @Translation("Greenwich Blocks")
 * )
 */
class GreenwichHeaderBlock extends BlockBase {



  /**
   * {@inheritdoc}
   */
  public function build() {
    // Retrieve the block settings.
    $settings = $this->getConfiguration();

    // block content goes here
    return [
      '#theme' => 'header',
      '#content' => 'test content',
      '#variant' => $settings['variant'] ?? 'website',
      '#title' => $settings['title'] ?? \Drupal::config('system.site')->get('name'),
      '#url' => $settings['url'] ??  \Drupal::config('system.site')->get('page.front', 'node') ,
      '#menu_main' => $this->getMenuOutput($settings['menu_main'] ?? 'main') ?? NULL,
      '#menu_aside' => $this->getMenuOutput($settings['menu_aside'] ?? 'header-aside') ?? NULL
    ];
  }

  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);


    // $menu_options = $this->getAllMenus();

    # title
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' =>  NULL,
      '#description' => $this->t('By default this is set to the same as the page title in <a href="@basic-site-settings">basic site settings</a>', array( '@basic-site-settings' => '/admin/config/system/site-information',))
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url'),
      '#default_value' => NULL,
      '#description' => $this->t('By default this is set to the frontpage setting defined in <a href="@basic-site-settings">basic site settings</a>', array( '@basic-site-settings' => '/admin/config/system/site-information',))
    ];

    $form['variant'] = [
      '#type' => 'select',
      '#title' => $this->t('Variant'),
      '#options' => ['website' => $this->t('Website'), 'gcd' => $this->t('Greenwich community directory')],
      '#default_value' => $this->configuration['variant'] ?? NULL,
      '#description' => $this->t('Choose from preconfigured options for the header')
    ];

    $form['menu_main'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select the main menu'),
      '#default_value' => $this->configuration['menu_main'] ?? NULL,
      '#description' => $this->t('The name of your main menu, defaults to `main`')
    ];

    $form['menu_aside'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select the aside menu'),
      '#default_value' => $this->configuration['menu_services'] ?? NULL,
      '#states' => [
        'visible' => [
           ':input[name="settings[variant]"]' => ['value' => 'website'],
        ],
      ],
      '#description' => $this->t('The name of the right hand side menu, defaults to `header_aside`')
    ];




    return $form;
  }

  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    // Save the block-specific settings.
    $this->configuration['title'] = $form_state->getValue('title') == '' ? NULL : $form_state->getValue('title');
    $this->configuration['url'] = $form_state->getValue('url') == '' ? NULL : $form_state->getValue('url');
    $this->configuration['variant'] = $form_state->getValue('variant');
    $this->configuration['menu_main'] = $form_state->getValue('menu_main') == '' ? NULL : $form_state->getValue('menu_main');
    $this->configuration['menu_aside'] = $form_state->getValue('menu_services') == '' ? NULL : $form_state->getValue('menu_services');
  }


  private function getMenuOutput($menu_name) {
    // Retrieve the block settings.
    $settings = $this->getConfiguration();

    // Load the menu using menu_tree service.
    $menu_tree = \Drupal::menuTree();

    // Define parameters for the menu tree.
    $parameters = new MenuTreeParameters();
    $parameters->setMaxDepth(2);

    // Load the menu tree.
    $tree = $menu_tree->load($menu_name, $parameters);

    // Build the menu render array.
    $menu_output = $menu_tree->build($tree);

    return $menu_output;
  }


  /**
   * Gets menu names available
   *
   * @param bool $check_access
   *   Only return menu names where the current user has access to "view label".
   *
   * @return array
   *   Menu Names available
   */
  // protected function getAllMenus(bool $check_access = TRUE) {
  //   $options = [];

  //   $eids =$this->entityTypeManager
  //     ->getStorage('menu')
  //     ->getQuery('AND')
  //     // The access check will check for "view" access. If we want to check
  //     // access, we want to check for "view label" access. So we'll disable
  //     // access checking for the query and check "view label" access separately.
  //     ->accessCheck(FALSE)
  //     ->execute();
  //   $menus = $this->entityTypeManager->getStorage('menu')->loadMultiple($eids);
  //   foreach ($menus as $name => $menu) {
  //     if ($check_access && !$menu->access('view label')) {
  //       continue;
  //     }
  //     $options[$name] = $menu->label();
  //   }
  //   asort($options);

  //   return $options;
  // }


}

