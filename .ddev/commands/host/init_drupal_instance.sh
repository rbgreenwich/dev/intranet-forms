#!/bin/bash

## Description: Setup environment for the first time
## Usage: init_drupal_instance [flags] [args]
## Example: "ddev init_drupal_instance db.sql" or "ddev init_drupal_instance"

# echo "🧙‍♂️ Updating composer file .... just in case"
# ddev composer update

echo "🪄 Installing things"
ddev composer install

if [ $# != 0 ]; then

  # @TODO add in checks here it fails with .gz files gunzip file.sql.qz
  echo "💿 Importing data to database"

  ddev drush sqlc <$1
  ddev drush cr
  ddev drush updb -y

fi

echo "📁 Importing configs to database"
ddev drush cr
ddev drush cim
ddev drush cr

echo "💪 You can now login"

ddev drush uli
