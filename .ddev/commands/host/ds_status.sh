#!/bin/bash

## Description: Just reruns the symlinks
## Usage: ds_status [flags] [args]
## Example: "ddev ds_status"

# quick undo rm -rf web/themes/custom/greenwich_base && rm -rf repos && ddev mutagen reset && ddev composer install

ddev exec ls -lah /var/www/html/web/themes/custom/greenwich_base
