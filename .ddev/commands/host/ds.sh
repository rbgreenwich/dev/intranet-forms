#!/bin/bash

## Description: Run command in repost/design_system ddev environment
## Usage: ds [flags] [args]
## Example: "ddev ds make install" or "ddev ds make watch-storybook-greenwich"

echo 'Commands are being run on ddev machine'

ddev exec --dir /var/www/html/repos/design_system nvm use --silent || nvm install $(cat .nvmrc)

# warn if there is no node_modules folder
if [ ! -d repos/design_system/node_modules ]; then
  echo "Warning: node_modules folder doesn't exist in your design_system repo;"

  while true; do
    read -p "Do you want to install node_modules? (y/n) " yn
    case $yn in
    [Yy]*)
      ddev exec --dir /var/www/html/repos/design_system make install-no-prereq
      break
      ;;
    [Nn]*)
      break
      ;;
    *) echo "Please answer yes or no." ;;
    esac
  done

fi

# Run the supplied commands in the design_system folder
ddev exec --dir /var/www/html/repos/design_system $@
