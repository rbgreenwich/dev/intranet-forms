#!/bin/bash

## Description: Just reruns the symlinks
## Usage: ds_symlinks [flags] [args]
## Example: "ddev ds_symlinks"
## Flags: [{"Name":"branch","Shorthand":"b","Type":"string","Usage":"Specify greenwich_base branch to check out, if blank the default branch will be used"},{"Name":"skip-checkout","Shorthand":"s","Type":"bool","Usage":"Skip the checkout and build phase of the greenwich_base repo"}]

# quick undo rm -rf web/themes/custom/greenwich_base && rm -rf repos && ddev mutagen reset && ddev composer install

REPO_URL="git@gitlab.com:rbgreenwich/dev/design_system.git"
skip_checkout=false

show_step() {
  echo '          '
  echo '          '
  echo "------------------------------------------------------"
  if [ ! -z "$1" ]; then
    echo '          '
    echo "     $1     "
  fi
  echo '          '
  echo '          '
}

has_argument() {
  [[ ("$1" == *=* && -n ${1#*=}) || (! -z "$2" && "$2" != -*) ]]
}

extract_argument() {
  echo "${2:-${1#*=}}"
}

handle_options() {
  while [ $# -gt 0 ]; do
    case $1 in
    -b | --branch*)
      if ! has_argument $@; then
        echo "No branch specified." >&2
        usage
        exit 1
      fi

      branch_name=$(extract_argument $@)

      shift
      ;;
    -s | --skip-checkout*)

      skip_checkout=true
      skip_checkout_flag=true

      shift
      ;;
    *)
      echo "Invalid option: $1" >&2
      usage
      exit 1
      ;;
    esac
    shift
  done
}

handle_options "$@"

symlink_theme() {
  show_step "Setup symlinks"
  # mkdir -p ${DDEV_APPROOT}/web/themes/custom/greenwich_base/theme
  # mkdir -p ${DDEV_APPROOT}/web/themes/custom/greenwich_base/patterns

  BACKUP_FOLDER=$(date +%Y-%m-%d-%H%M%S)
  ddev exec mkdir --parents repos/bkup/${BACKUP_FOLDER}
  ddev exec mv web/themes/custom/greenwich_base/theme repos/bkup/${BACKUP_FOLDER}
  ddev exec mv web/themes/custom/greenwich_base/patterns repos/bkup/${BACKUP_FOLDER}
  ddev exec ln -sfnv ../../../../repos/design_system/src/themes/drupal/greenwich_base web/themes/custom/greenwich_base/theme
  ddev exec ln -sfnv ../../../../repos/design_system/dist/themes/drupal/greenwich_base web/themes/custom/greenwich_base/patterns
  # ddev exec ls -lah /var/www/html/web/themes/custom/greenwich_base
}

# prep_step

symlink_theme

ddev drush cr
