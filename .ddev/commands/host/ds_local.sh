#!/bin/bash

## Description: Run command in repost/design_system folder locally
## Usage: ds_local [flags] [args]
## Example: "ddev dsl make install" or "ddev dsl make watch-storybook-greenwich"

echo 'Commands are being run on your local machine'
cd ${DDEV_APPROOT}/repos/design_system
# run the command
$@
