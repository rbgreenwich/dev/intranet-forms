#!/bin/bash

## Description: Setup environment for theme development
## Usage: ds_undo [flags] [args]
## Example: "ddev ds_undo -k" or "ddev ds_undo"
## Flags: [{"Name":"keep-repo","Shorthand":"k","Type":"bool","Usage":"Optionally keep the repos/greenwich_base folder"}]

keep_repo=false
keep_repo_flag=false

has_argument() {
  [[ ("$1" == *=* && -n ${1#*=}) || (! -z "$2" && "$2" != -*) ]]
}

extract_argument() {
  echo "${2:-${1#*=}}"
}

handle_options() {
  while [ $# -gt 0 ]; do
    case $1 in
    -k | --keep-repo*)
      keep_repo=true
      keep_repo_flag=true

      shift
      ;;
    *)
      echo "Invalid option: $1" >&2
      usage
      exit 1
      ;;
    esac
    shift
  done
}

handle_options "$@"

if [ "$keep_repo_flag" = false ]; then
  while true; do
    read -p "Do you want to keep your repos/design_system folder? (y/n) " yn
    case $yn in
    [Yy]*)
      keep_repo=true
      break
      ;;
    [Nn]*)
      keep_repo=false
      break
      ;;
    *) echo "Please answer yes or no." ;;
    esac
  done
fi

rm -rf web/themes/custom/greenwich_base

mv repos/bkup/greenwich_base/theme web/themes/custom/greenwich_base
mv repos/bkup/greenwich_base/patterns web/themes/custom/greenwich_base
rm -rf repos/bkup

rm .gitmodules

if [ "$keep_repo" = false ]; then
  rm -rf repos
fi

ddev mutagen reset
ddev composer install
