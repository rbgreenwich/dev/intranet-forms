# Greenwich Community Directory - Drupal site

A Composer-based installer based on the LocalGov Drupal Composer project
template for the LocalGov Drupal distribution.

## Quick start

### 1. Clone this repository

```sh

git clone git@gitlab.com:rbgreenwich/dev/intranet-forms.git
cd intranet-forms
```

### 2. Download a copy of the Drupal database.

Currently easiest to log into the dev site and use Backup Migrate to download a copy of the database.

https://gcd.dev.royalgreenwich.gov.uk/admin/config/development/backup_migrate

### 3. Use DDEV to run local webserver.

Boot up DDEV, run composer install and import the database and latest config.

```sh
ddev start # Start ddev.
ddev composer install # Install all required code packages with composer.
ddev import-db -f database.sql.gz # Import the database with DDEV.
ddev drush cr # Cache rebuild, using drush.
ddev drush updb # Run database updates with Drush.
ddev drush cim # Import the config, with Drush.
ddev drush uli # Generate a one time log in link, with Drush.
```

Note, there is also a helper command that will run the equivalent of the above comands:

```sh
ddev init_drupal_instance database.sql
```

If you wish to work on the greenwich_base theme - see below for instructions on configuring that setup

# Restoring from dev

Open the dev environment and create a **quick backup**. `admin > configuration > development > backup and migrate`.

```sh
# ensure your environment is up to date

git checkout develop

git pull

# replace current db database with the a new one
ddev import-db --no-progress --file=backup8.mysql

ddev drush cr

# import any local changes
ddev drush cim

# because it never hurts to clear the cache!
ddev drush cr

```

# Enabling theme development

Theme development is optionally enabled, when not enabled the `base_theme` is sourced from the gitlab [greenwich_base](https://gitlab.com/rbgreenwich/packages/greenwich_base) package, which is built from the [design system](https://gitlab.com/rbgreenwich/design_system).

There are two ways to enable theme development, the theme development mode replaces the greenwich_base theme with symlinks to the built folders in the `repos/design_system` folder.

```sh

# initialise base theme
ddev ds_init

# show help menu
ddev ds_init -h

# initialise base theme without overwriting the repo - it also skips some of the questions around checkout and branches
ddev ds_init -s

# initialise and switch branches
# -b cannot be passed with -s since theres a risk of messing with your branch
ddev ds_init -b feature/header

# run each of these individually
ddev ds make watch-theme-greenwich
ddev ds make watch-storybook-greenwich

open http://intranet-forms.ddev.site:3009

# if you have issues accessing storybook try
ddev dsl make watch-storybook-greenwich

# or run this command to have everything build in dev mode (nb this uses tmux)
ddev ds make start-greenwich

```

You can run `ddev ds_status` to check the status of the symlinks

```sh
♥ ddev ds_status
total 24K
drwxr-xr-x 3 han dialout 4.0K Dec 19 14:05 .
drwx------ 4 han dialout 4.0K Dec 15 01:35 ..
drwxr-xr-x 8 han dialout 4.0K Dec 15 01:57 .git
-rw-r--r-- 1 han dialout  237 Dec 15 01:35 composer.json
lrwxrwxrwx 1 han dialout   65 Dec 19 14:05 patterns -> ../../../../repos/design_system/dist/themes/drupal/greenwich_base
lrwxrwxrwx 1 han dialout   64 Dec 19 14:05 theme -> ../../../../repos/design_system/src/themes/drupal/greenwich_base
```

You can also run the `ddev ds_symlinks` and `ddev ds_symlinks_undo` commands for a more manual approach.

For information on how to use the theme see the documentation on that repo or run `ddev theme help`.

| command                                  | action                                                                     |
| ---------------------------------------- | -------------------------------------------------------------------------- |
| `ddev ds make help`                      | List all commands                                                          |
| `ddev ds make install`                   | Installs the project                                                       |
| `ddev ds make start`                     | Starts the drupal component library storybook                              |
| `ddev ds make start-greenwich`           | Starts the everything you need for drupal development                      |
| `ddev ds make generate-component`        | Creates a component, see below for more information                        |
| `ddev ds make watch-storybook-greenwich` | Loads greenwich design system storybook development task                   |
| `ddev ds make watch-theme-greenwich`     | Loads design_system development task to create assets for the drupal theme |
| `ddev ds make build-storybook-greenwich` | Builds greenwich design system storybook                                   |
| `ddev ds make build-theme-greenwich`     | Builds design_system for the drupal theme                                  |
| `ddev ds make build-greenwich-base`      | Builds the final artifacts for the drupal theme package                    |
| `ddev ds yarn add -D glob`               | Add a package                                                              |

## Uninstall the theme

```sh
ddev ds_undo

# show help menu
ddev ds_undo -h

# keep the code under /repo
ddev ds_undo -k

```

# ds

`ds` will run the command passed to it on the **ddev instance**

```sh
ddev ds make watch-storybook-greenwich
```

# dsl

`ds` will run the command passed to it in the repost/design_system folder on your **local machine**

```sh
ddev dsl make watch-storybook-greenwich
```

# exec_d

```sh
# run a command in the folder your in
ddev exec_d <cmd>

# eg
cd repos/design_system
ddev exec_d -D glob
```

# Gitpod

We should also be able to start this in Gitpod.

[Open in Gitpod](https://gitpod.io/#https://gitlab.com/rbgreenwich/gcd/gcd-drupal/)

# Usage

This repository comes with the root composer file which attempts to define all
code dependencies. Cloning the repo and running comploser install should bring
all code up to the same point.

We also have configuration files for DDEV, to help with a fast
and consistent local development setup.

You can also optionally enable the greenwich_base theme if you need to work on it. Read below for instructions on how to enable this.

### Import a database

Then it is super useful to import a copy of the databse so that we can import
updated configuration for the site and collaborate with other developers.

For now we can start with this.

```sh
wget database.sql
ddev drush sqlc < database.sql
ddev drush cr
ddev drush updb -y
```

### Import configs

Import any new config.

```sh
ddev drush cim
```

(If required export any config before committing your work)

```sh
ddev drush cim
```

### Clear cache

Your most used command...

Now clear the cache and generate a log in link.

```sh
ddev drush cr
```

### Generate admin login link

No passwords needed!

```sh
ddev drush uli
```

### Launch the site

And always rebuild the cache!

## Wingsuit

To start theming on the Wingsuit theme, we should be able to build the node
modules and start developing in Storybook and Drupal as follows.

The design_system repo contains wingsuit / storybook and the drupal base theme named wingsuit.

```sh
cd web/themes/custom/greenwich_base/
git pull origin main
ddev yarn
ddev yarn dev:storybook
ddev drush launch
```

## Gitlab tokens to require private packages or repos.

Some packages or repos required by the root composer project might be private.
With Gitlab we need to generate personal access tokens to allow composer to
access the wothout further authentication.

To do this first generate a personal access token with at least api and read_api
permisssion. For example, follow this link:

https://gitlab.com/-/profile/personal_access_tokens?name=Example+Access+token&scopes=api,read_api

Once you've created the access token, copy it down as you won't be able to see
it again.

Then we can configure composer inside the ddev container to us it with:

```
ddev composer config --global --auth gitlab-token.gitlab.com {you personal acceses token}
```

## Branching methodology and deploying to the dev site

We are aiming to use the 'release flow' branching methodology.

See: http://releaseflow.org/

For now, we are using `develop` as the mainline branch.

So for a new feature, we might start by creating an issue in Gitlab.

For example, when adding the Backup Migrate module and configuration, we created the issue https://gitlab.com/rbgreenwich/gcd/gcd-drupal/-/issues/2

We then create a feature branch following the naming convention feature/[issue number]-[description] `feature/2-backup-migrate`.

```
git checkout develop
git pull origin develop
git checkout -b feature/2-backup-migrate
```

Locally we committed changes to code and config.

```
ddev composer require drupal/backup-migrate
ddev drush en backup-migrate
ddev drush cex
git status
git add config/ composer.json composer.lock
git commit -m 'Add backup_migrate module for developers.'
git push origin feature/2-backup-migrate
```

Gitlab then provides and handy link to generate the merge request.

Either use this link or manually create a merge request for the feature branch back into the develop branch.

Once any needed review has taken place, merge the merge request into develop.

This will trigger the deploy to the dev site.

If it doesn't work, ask Chris on Teams.

# Troubleshooting / debugging

See [TROUBLESHOOTING.md](./TROUBLESHOOTING.md)

# TODO

- [ ] https://medium.com/packagist/custom-package-definitions-3f433629861e
- [ ] https://stackoverflow.com/questions/76530637/gitlab-how-to-include-build-artifacts-when-adding-a-package-via-ci-pipeline-to
- [ ] https://stackoverflow.com/questions/29013457/how-to-store-releases-binaries-in-gitlab
