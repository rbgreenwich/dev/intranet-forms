# Troubleshooting

Please contribute troubleshooting steps for any issues you encounter.

## Container network problems

As detailed in [This StackOverflow question](https://stackoverflow.com/questions/20430371/my-docker-container-has-no-internet/45644890#45644890), the container's DNS settings (defined in /etc/resolv.conf) may be faulty. You can manually define the DNS for all the hosts' containers by editing /etc/docker/daemon.json:

```
{
    "dns": ["8.8.8.8"]
}
```

8.8.8.8 in this case is a DNS server hosted by Google, but others should work.
